#include <iostream>
#include <fstream>
#include <vector>
#include <limits>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <algorithm>

using namespace std;

/**
  * Programming Challenge 1: Name Generator
  * Sean Castillo 2019
  *
  * Task: Generates names
  * To use: The directory must have three text files: firstNames.txt, lastNames.txt, and credentials.txt
  *         Each file should use 'one name per line' formatting.
  */


/**
 * function to read the lines in a given name file and put them into a vector, which is returned
 */
std::vector<string> readInNames(string fileToCheck)
{
    std::vector<std::string> nameLines;

    // read the text files
    ifstream nameFile;
    nameFile.open(fileToCheck);

    // check if the input file was actually opened
    if (!nameFile.is_open())
    {
        cout << "Error: " << fileToCheck << " not found in the directory!\n";
        return nameLines;
    }

    // check for empty file
    nameFile.seekg(0, ios_base::end);
    unsigned int len = nameFile.tellg();
    if (len == 0)
    {
        cout << "Error: " << fileToCheck << " is empty!\n";
        return nameLines;
    }

    // resets the stream to beginning of file
    nameFile.seekg(0, ios_base::beg);

    // put the names into a vector
    std::string line;

    while (std::getline(nameFile, line))
    {
        nameLines.push_back(line);
    }

    // close the file
    nameFile.close();

    // export the vector
    return nameLines;
}


/**
 *  function to print the contents of the given vector, headed by the given listHeaderText
 */
void printNames(std::string listHeaderText, std::vector<std::string> v)
{
    cout << listHeaderText << endl;
    for (unsigned int i=0; i<v.size(); i++) { cout << v[i] << endl; }
    cout << endl;
}


/**
 *  function to get number of names to generate
 */
unsigned int getNumberOfNames()
{
    unsigned int num = 0;

    nameNumberInputLoopLabel:
    cout << "How many names should I generate?" << endl;
    cin >> num;

    while (cin.fail())
    {
        cout << "That's not a number!" << endl;
        //clear the fail flag
        cin.clear();
        //use this because cin leaves a newline in the stream
        cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        goto nameNumberInputLoopLabel;
    }
    cout << endl;
    return num;
}

/**
 * helper function to return 1 given (Y)es or return 0 given (N)o
 */
bool yesNoAnswerHandler()
{
    string answer;
    inputLoopLabel:
    cin >> answer;
    std::for_each(answer.begin(), answer.end(), [](char & c)
    {   c = ::toupper(c); });

    if      (answer == "Y" || answer == "YES"){ return true; }
    else if (answer == "N" || answer == "NO") { return false;}
    else
    {
        cout << "Unexpected answer. Try again." << endl;
        cin.clear();
        cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        goto inputLoopLabel;
    }
}

/**
 * function to get a name at random given a vector
 */
std::string getName(std::vector<std::string> v)
{
    string name = "";
	srand(time(NULL));
    name = v[rand() % v.size()];
    return name;
}

void generateNames(unsigned int numNames, bool useCredentials, std::vector<std::string> firstNames,
                   std::vector<std::string> lastNames, std::vector<std::string> credentials)
{
    for (unsigned int i=0; i<numNames; i++)
    {
        cout << getName(firstNames) << " " << getName(lastNames);
        if (useCredentials) { cout << " " << getName(credentials); }
        cout << endl;
    }
}

 /**
  * function to call the shots
  */
int main()
{
    // make vectors with the names
    std::vector<std::string> firstNames  = readInNames("firstNames.txt" );
    std::vector<std::string> lastNames   = readInNames("lastNames.txt"  );
    std::vector<std::string> credentials = readInNames("credentials.txt");

    // return -1 if a list is empty or not found
    if (firstNames.size() == 0 || lastNames.size() == 0 || credentials.size() == 0)
    { return -1; }

    printNames("First Names:", firstNames );
    printNames("Last Names:",  lastNames  );
    printNames("Credentials:", credentials);

    mainLoop:
    // get the number of names to generate
    unsigned int numNames = getNumberOfNames();

    // check if the user wants to use credentials
    cout << "Use credentials for the names? (Y)es / (N)o" << endl;
    bool useCredentials = yesNoAnswerHandler();

    // generate the names
    cout << "Generating " << numNames << " name(s)..." << endl;
    generateNames(numNames, useCredentials, firstNames, lastNames, credentials);

    // ask the user whether to run again
    cout << "Go again? (Y)es / (N)o" << endl;
    bool goAgain = yesNoAnswerHandler();
    if (goAgain) { goto mainLoop; }

    // exit
    return 0;
}
